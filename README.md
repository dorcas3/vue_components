# List Of Interns

## User Requirements
- Create a simple single page application with three pages;
    - Page one will be a  welcome page
    - Page two will be a page showing the list of interns
    - Page three will be a page showing a specific intern and their stacks
- Each of the pages should have at least two components. We shall focus on the old options api rendering and not the new composition API.

## Project Screenshots

![Home](assets/homepage.png) ![Interns Page](assets/internspage.png) 
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

