export default [
    {
        id: 1,
        name: "Dorcas Cherono",
        email: "dorcas@sendyit.com",
        stacks: ["Vue", "Adonis"],
      },

      {
        id: 2,
        name: "Stacey Chebet",
        email: "stacey@sendyit.com",
        stacks: ["Flutter", "Adonis"],
      },
      {
        id: 3,
        name: "Maxwell Kiprop",
        email: "Maxwell@sendyit.com",
        stacks: ["Go", "Flutter"],
      },
      {
        id: 4,
        name: "Gill Erick",
        email: "Gill@sendyit.com",
        stacks: ["Quarkus", "Vue"],
      },
]