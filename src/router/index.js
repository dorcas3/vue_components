import { createWebHistory, createRouter } from "vue-router"
import Intern from '../components/Intern.vue'
import Home from '../components/Home.vue'
import InternDetails from '../components/Intern-details.vue'
import PageNotFound from '../components/PageNotFound.vue'



const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/interns",
    name: "Intern",
    component: Intern,
  },
  {
    path: "/intern/:name",
    name: "InternDetails",
    component: InternDetails,
  },
  {
    path: '/:catchAll(.*)*',
    name: "PageNotFound",
    component: PageNotFound,
  },
]
const router = createRouter({

  history: createWebHistory(),
  routes
})

export default router